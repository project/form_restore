
Note: Use this module with add_n_reference.module
      (see ../add_n_reference/README.txt for details)

This module restores form values saved in $_SESSION variable
on non-submit form actions. Values are free for altering
so You can use it to simulate multi-step workflow.
